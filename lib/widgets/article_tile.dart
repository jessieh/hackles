// widgets/article_tile.dart
// Provides a ListTile Widget representing a Hacker News article

/* -------------------------------------------------------------------------- */
/* Package imports */

import 'package:flutter/material.dart';

import 'package:share_plus/share_plus.dart';
import 'package:url_launcher/url_launcher.dart';

/* -------------------------------------------------------------------------- */
/* Local imports */

import '../models/article.dart';

import '../widgets/article_icon.dart';
import '../widgets/tile.dart';

/* -------------------------------------------------------------------------- */
/* Constants */

// This really isn't how you're supposed to accomplish this...
// ...but this is a small project, so whatevs:

/// A [Map] of [Article]s, keyed by their IDs.
///
/// Used to avoid having to fetch Article data every time an [ArticleTile]
/// scrolls back into view and needs to be rebuilt.
final Map<int, Article> __articleCache = {};

/* -------------------------------------------------------------------------- */
/* ArticleTile class */

/// A [ListTile] [Widget] representing a Hacker News article.
///
/// Has a content-sensitive icon, and offers two behaviors: opening [Article.url]
/// in a browser on tap, and sharing [Article.url] when long pressed.
class ArticleTile extends StatefulWidget
{

  /* ---------------------------------- */
  /* Member fields */

  /// The unique ID of the article that this element will display the data of
  final int articleID;

  /* ---------------------------------- */
  /* Constructor */

  const ArticleTile( {
      Key? key,
      required this.articleID
  } )
  : super(
    key: key
  );

  /* ---------------------------------- */
  /* createState */

  @override
  _ArticleTileState createState() => _ArticleTileState();

}

/* -------------------------------------------------------------------------- */
/* _ArticleTileState class */

class _ArticleTileState extends State<ArticleTile>
{

  /* ---------------------------------- */
  /* Member fields */

  /// [Future] that will resolve to the [Article] once its data has been fetched
  late Future<Article?> futureArticle;

  /* ---------------------------------- */
  /* _buildArticleTile */

  /// Assembles the actual [Widget] that will be displayed once [futureArticle] has resolved.
  ///
  /// The [Tile] is assembled using the data from [article].
  Widget _buildArticleTile( BuildContext context, Article article )
  {
    return Tile(

      leading: ArticleIcon( article ),
      title: Text( article.title ),
      subtitle: Text( Uri.parse(article.url).host ),
      trailing: Text(
        article.score.toString(),
        style: TextStyle(
          color: article.score > 300 ? Theme.of( context ).colorScheme.primary : Theme.of( context ).colorScheme.tertiary,
          fontWeight: article.score > 300 ? FontWeight.bold : FontWeight.normal,
        )
      ),

      onTap: ()
      {
        launch( article.url );
      },

      onLongPress: ()
      {
        Share.share( article.url );
      },

    );
  }

  /* ---------------------------------- */
  /* initState */

  @override
  void initState()
  {
    super.initState();
    futureArticle = Article.fetch( widget.articleID );
  }

  /* ---------------------------------- */
  /* build */

  @override
  Widget build( BuildContext context )
  {

    // If the data for this article has been retrieved before, don't bother waiting for the
    // Future to resolve-- just return the constructed Widget
    if ( __articleCache.containsKey( widget.articleID ) )
    {
      return _buildArticleTile( context, __articleCache[ widget.articleID ]! );
    }

    return FutureBuilder<Article?>(

      future: futureArticle,

      builder: ( context, snapshot )
      {

        if ( snapshot.hasData )
        {
          __articleCache[ widget.articleID ] = snapshot.data!;
          return _buildArticleTile( context, snapshot.data! );
        }

        else if ( snapshot.hasError )
        {
          return Tile(

            leading: const Icon( Icons.sync_problem ),
            title: const Text( 'Unable to retrieve article data' ),
            subtitle: const Text( 'Tap to retry' ),

            // This triggers a rebuild of the ArticleTile
            onTap: ()
            {
              futureArticle = Article.fetch( widget.articleID );
              setState( () {} );
            },

          );
        }

        else
        {
          return const Tile(
            leading: CircularProgressIndicator(),
            title: Text( '...' ),
            subtitle: Text( '...' ),
          );

        }

      },

    );

  }

}

// widgets/tile.dart
// Provides a generic, pre-formatted ListTile Widget with gesture callbacks

/* -------------------------------------------------------------------------- */
/* Package imports */

import 'package:flutter/material.dart';

/* -------------------------------------------------------------------------- */
/* Tile class */

/// A pre-formatted [ListTile] [Widget] with gesture callbacks.
class Tile extends StatefulWidget
{

  /* ---------------------------------- */
  /* Member fields */

  /// A Widget to display before the title.
  final Widget? leading;

  /// The primary content of the element.
  final Widget? title;

  /// Additional content displayed below the title.
  final Widget? subtitle;

  /// A Widget to display after the title.
  final Widget? trailing;

  /// Called when the user taps this element.
  final void Function()? onTap;

  /// Called when the user long presses this element.
  final void Function()? onLongPress;

  /* ---------------------------------- */
  /* Constructor */

  const Tile( {
      Key? key,
      this.leading,
      this.title,
      this.subtitle,
      this.trailing,
      this.onTap,
      this.onLongPress,
  } )
  : super(
    key: key
  );

  /* ---------------------------------- */
  /* createState */

  @override
  _TileState createState() => _TileState();

}

/* -------------------------------------------------------------------------- */
/* _TileState class */

class _TileState extends State<Tile>
{

  /* ---------------------------------- */
  /* build */

  @override
  Widget build( BuildContext context )
  {

    return Padding(

      padding: const EdgeInsets.symmetric(
        horizontal: 8.0,
        vertical: 4.0,
      ),

      child: GestureDetector(

        onLongPress: ()
        {
          if ( widget.onLongPress != null )
          {
            widget.onLongPress!();
          }
        },

        child: ListTile(

          leading: widget.leading,
          title: widget.title,
          subtitle: widget.subtitle,
          trailing: widget.trailing,

          onTap: () {
            if ( widget.onTap != null )
            {
              widget.onTap!();
            }
          },

          iconColor: Theme.of( context ).colorScheme.secondary,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular( 32.0 ),
          ),
          contentPadding: const EdgeInsets.symmetric(
            horizontal: 16.0,
            vertical: 4.0,
          ),

        ),

      ),

    );

  }

}

// widgets/article_icon.dart
// Provides aa contextual Icon Widget representing a Hacker News article

/* -------------------------------------------------------------------------- */
/* Package imports */

import 'package:flutter/material.dart';

import 'package:font_awesome_flutter/font_awesome_flutter.dart';

/* -------------------------------------------------------------------------- */
/* Local imports */

import '../models/article.dart';

/* -------------------------------------------------------------------------- */
/* ArticleIcon class */

/// An context-sensitive [Icon] [Widget] representing a Hacker News article.
class ArticleIcon extends StatefulWidget
{

  /* ---------------------------------- */
  /* Member fields */

  /// The [Article] that this element will build a representative icon for.
  final Article article;

  /* ---------------------------------- */
  /* Constructor */

  const ArticleIcon(
    this.article,
    {
      Key? key,
    }
  )
  : super(
    key: key
  );

  /* ---------------------------------- */
  /* createState */

  @override
  _ArticleIconState createState() => _ArticleIconState();

}

/* -------------------------------------------------------------------------- */
/* _ArticleIconState class */

class _ArticleIconState extends State<ArticleIcon>
{

  /* ---------------------------------- */
  /* _getArticleIconData */

  /// Returns an appropriate [IconData] to represent the class or content of the
  /// [Article], depending on its type, URL, or title.
  IconData _getArticleIconData( Article article )
  {
    switch( article.type )
    {

      // Job listing
      case 'job':
      return Icons.work;

      // Comment (this should never show up, but we've got this here just in case...)
      case 'comment':
      return Icons.forum;

      // Community poll
      case 'poll':
      return Icons.poll;

      // Poll option (again, another thing that should never show up...)
      case 'pollopt':
      return Icons.check_box;

      // Story/everything else
      case 'story':
      default:
      {

        final title = article.title.toLowerCase();
        final url = article.url.toLowerCase();
        final host = Uri.parse( article.url ).host.toLowerCase();

        if ( title.contains( 'ask hn:' ) )
        {
          return Icons.contact_support;
        }
        else if ( title.contains( 'tell hn:' ) )
        {
          return Icons.comment;
        }
        else if ( title.contains( 'show hn:' ) )
        {
          return FontAwesomeIcons.shareAltSquare;
        }
        else if ( url.contains( 'https://news.ycombinator.com/item' ) )
        {
          return Icons.forum;
        }
        else if ( title.contains( '[pdf]' ) || url.endsWith( '.pdf' ) )
        {
          return Icons.description;
        }
        else if ( host.contains( 'github.com' ) || host.contains( 'gitlab.com' ) )
        {
          return FontAwesomeIcons.codeBranch;
        }
        else if ( host.contains( 'twitter.com' ) )
        {
          return FontAwesomeIcons.twitter;
        }
        else if ( host.contains( 'reddit.com' ) )
        {
          return FontAwesomeIcons.redditAlien;
        }
        else
        {
          return Icons.auto_stories;
        }

      }

    }
  }

  /* ---------------------------------- */
  /* build */

  @override
  Widget build( BuildContext context )
  {
    return Icon( _getArticleIconData( widget.article ) );
  }

}

// main.dart
// Application entry point

/* -------------------------------------------------------------------------- */
/* Package imports */

import 'package:flutter/material.dart';

/* -------------------------------------------------------------------------- */
/* Local imports */

import './views/home.dart';

/* -------------------------------------------------------------------------- */
/* Constants */

/// The primary theme color of the application. Duh!
///
/// The application theme is generated from this color. That means that you can
/// change the entire look of this application by changing this variable. Isn't
/// Flutter seriously so neat?
///
/// Also: #FF6600 is Hacker News orange. :)
const Color primaryThemeColor = Color( 0xFFFF6600 );

/* -------------------------------------------------------------------------- */
/* App class */

/// A [MaterialApp] that provides the top level of the [Widget] tree.
///
/// Also responsible for providing theme information.
class App extends StatelessWidget {

  /* ---------------------------------- */
  /* Constructor */

  const App( { Key? key } ) : super( key: key );

  /* ---------------------------------- */
  /* _themeFromColor */

  /// Generates a [ThemeData] from [seedColor] and [brightness].
  ///
  /// [seedColor] will determine the color scheme, and [brightness]
  /// determines whether or not the theme will a light or dark theme.
  ThemeData _themeFromColor( Color seedColor, Brightness brightness )
  {

    final ColorScheme colorScheme = ColorScheme.fromSeed(
      seedColor: seedColor,
      brightness: brightness,
    );

    return ThemeData.from(
      colorScheme: colorScheme,
    ).copyWith(
      androidOverscrollIndicator: AndroidOverscrollIndicator.stretch,
    );

  }

  /* ---------------------------------- */
  /* build */

  @override
  Widget build( BuildContext context )
  {
    return MaterialApp(

      title: 'Hackles',

      themeMode: ThemeMode.system,
      theme: _themeFromColor( primaryThemeColor, Brightness.light ),
      darkTheme: _themeFromColor( primaryThemeColor, Brightness.dark ),

      home: const Home(),

    );
  }

}

/* -------------------------------------------------------------------------- */
/* main */

void main()
{
  runApp(
    const App()
  );
}

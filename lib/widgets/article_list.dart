// widgets/article_list.dart
// Provides a Sliver Widget representing a list of Hacker News articles

/* -------------------------------------------------------------------------- */
/* Package imports */

import 'package:flutter/material.dart';

/* -------------------------------------------------------------------------- */
/* Local imports */

import '../models/top_article_ids.dart';

import '../widgets/article_tile.dart';

/* -------------------------------------------------------------------------- */
/* ArticleList class */

/// A Sliver [Widget] representing a list of Hacker News articles as [ArticleTile]s.
class ArticleList extends StatefulWidget
{

  /* ---------------------------------- */
  /* Constructor */

  const ArticleList( { Key? key } ) : super( key : key );

  /* ---------------------------------- */
  /* createState */

  @override
  _ArticleListState createState() => _ArticleListState();

}

/* -------------------------------------------------------------------------- */
/* _ArticleListState class */

class _ArticleListState extends State<ArticleList>
{

  /* ---------------------------------- */
  /* Member fields */

  /// [Future] that will resolve to the [TopArticleIDs] list once its data has been fetched
  late Future<TopArticleIDs> futureArticleIDs;

  /* ---------------------------------- */
  /* _buildSuggestions */

  /// Builds [SliverList] of [ArticleTile]s for each articleID in [articleIDs]
  Widget _buildSuggestions( articleIDs )
  {
    return SliverList(
      delegate: SliverChildBuilderDelegate(

        ( BuildContext context, int index )
        {
          return ArticleTile(
            articleID: articleIDs[ index ]
          );
        },

        childCount: articleIDs.length

      ),
    );
  }

  /* ---------------------------------- */
  /* initState */

  @override
  void initState()
  {
    super.initState();
    futureArticleIDs = TopArticleIDs.fetch();
  }

  /* ---------------------------------- */
  /* build */

  @override
  Widget build( BuildContext context )
  {
    return FutureBuilder<TopArticleIDs>(

      future: futureArticleIDs,

      builder: ( context, snapshot )
      {

        if ( snapshot.hasData )
        {
          return _buildSuggestions( snapshot.data! );
        }

        else if ( snapshot.hasError )
        {
          return const SliverFillRemaining(
            hasScrollBody: false,
            child: Center(
              child: Text( 'Unable to retrieve Hacker News front page' ),
            ),
          );
        }

        else
        {
          return const SliverFillRemaining(
            hasScrollBody: false,
            child: Center(
              child: CircularProgressIndicator(),
            ),
          );
        }

      },

    );
  }

}

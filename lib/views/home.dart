// views/home.dart
// Provides Scaffold for the primary screen of the application

/* -------------------------------------------------------------------------- */
/* Package imports */

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

/* -------------------------------------------------------------------------- */
/* Local imports */

import '../widgets/article_list.dart';

/* -------------------------------------------------------------------------- */
/* Home class */

/// A [Scaffold] [Widget] representing the primary screen of the application.
///
/// Also responsible for applying the primary [SystemUiOverlayStyle] settings
/// to the system status bar and navigation bar.
class Home extends StatefulWidget
{

  /* ---------------------------------- */
  /* Constructor */

  const Home( { Key? key } ) : super( key : key );

  /* ---------------------------------- */
  /* createState */

  @override
  _HomeState createState() => _HomeState();

}

/* -------------------------------------------------------------------------- */
/* _HomeState class */

class _HomeState extends State<Home>
{

  /* ---------------------------------- */
  /* build */

  @override
  Widget build( BuildContext context )
  {

    // Update the system status bar and navigation bar to match the app's theme
    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
        systemNavigationBarColor: Theme.of( context ).colorScheme.background,
      )
    );

    return Scaffold(
      body: CustomScrollView(
        slivers: <Widget>[

          // Dynamic application bar (top bar)
          SliverAppBar(

            pinned: true,
            expandedHeight: 160,
            shadowColor: Colors.transparent,

            // App title
            flexibleSpace: const FlexibleSpaceBar(
              expandedTitleScale: 1.75,
              title: Text(
                'Hackles',
                style: TextStyle(
                  fontWeight: FontWeight.normal
                )
              ),
            ),

            // Refresh button
            actions: <Widget>[
              IconButton(

                icon: const Icon(Icons.refresh_outlined),
                tooltip: 'Refresh articles',

                // This just triggers a rebuild of the Home component
                onPressed: ()
                {
                  setState( () {} );
                },

              )
            ],

          ),

          // List view containeer
          SliverPadding(

            padding: const EdgeInsets.symmetric(
              vertical: 4.0,
            ),

            // We generate a UniqueKey for the ArticleList every time
            // this component is rebuilt. This forces Flutter to rebuild
            // the article list in its entirety, triggering a full refresh
            sliver: ArticleList(
              key: UniqueKey()
            ),

          ),

        ],
      ),
    );

  }

}

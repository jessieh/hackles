// models/top_article_ids.dart
// Provides data model for front page of Hacker News

/* -------------------------------------------------------------------------- */
/* Language imports */

import 'dart:async';
import 'dart:collection';
import 'dart:convert';

/* -------------------------------------------------------------------------- */
/* Package imports */

import 'package:http/http.dart' as http;

/* -------------------------------------------------------------------------- */
/* TopArticleIDs class */

class TopArticleIDs extends ListBase<int>
{

  /* ---------------------------------- */
  /* Member fields */

  /// The list of articles (ID only) that are on the front page of Hacker News.
  final List<int> articleIDs;

  /* ---------------------------------- */
  /* Constructors */

  /// Constructs a [TopArticleIDs] list from the contents of [list].
  TopArticleIDs( list ) : articleIDs = list;

  /* ---------------------------------- */
  /* ListBase operations */

  /// Sets the length of the underlying list.
  @override
  set length( int newLength ) { articleIDs.length = newLength; }

  /// Returns the length of the underlying list.
  @override
  int get length => articleIDs.length;

  /// Returns the articleID at index [index] from the underlying list.
  @override
  int operator []( int index ) => articleIDs[ index ];

  /// Sets the articleID at index [index] in the underlying list.
  @override
  void operator []=( int index, int value ) { articleIDs[ index ] = value; }

  /* ---------------------------------- */
  /* Static methods */

  /* ---------------------------------- */
  /* fetch */

  /// Attempts to return an instance of [TopArticleIDs] containing the current data for the front page of Hacker News.
  ///
  /// Returns an empty instance of [TopArticleIDs] if the information could not be retrieved.
  static Future<TopArticleIDs> fetch() async
  {

    final response = await http.get( Uri.parse( 'https://hacker-news.firebaseio.com/v0/topstories.json' ) );

    if ( response.statusCode == 200 )
    {
      final articleIds = TopArticleIDs( jsonDecode( response.body ).cast<int>() );
      if ( articleIds.length > 100 )
      {
        articleIds.length = 100;
      }
      return articleIds;
    }
    else
    {
      return TopArticleIDs( <int>[] );
    }

  }

}

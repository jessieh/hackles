// models/article.dart
// Provides model for Hacker News articles

/* -------------------------------------------------------------------------- */
/* Language imports */

import 'dart:async';
import 'dart:convert';

/* -------------------------------------------------------------------------- */
/* Package imports */

import 'package:http/http.dart' as http;

/* -------------------------------------------------------------------------- */
/* Article class */

/// Metadata for an article that has been posted or shared to Hacker News.
class Article
{

  /* ---------------------------------- */
  /* Member fields */

  /// The unique ID of the article.
  final int id;

  /// The score of the article, as voted on by Hacker News readers.
  final int score;

  /// The title of the article.
  final String title;

  /// The type of article.
  ///
  /// Possible types: "job", "story", "comment", "poll", or "pollopt".
  final String type;

  /// The URL of the article.
  final String url;

  /* ---------------------------------- */
  /* Constructor */

  const Article( {
      required this.id,
      required this.score,
      required this.title,
      required this.type,
      required this.url,
  } );

  /* ---------------------------------- */
  /* Constructor (fromJson) */

  /// Constructs an [Article] from the provided [json].
  ///
  /// If a URL is not provided for the article, then a link to
  /// the article on Hacker News will be substituted. Any
  /// required fields that are not present in [json] will
  /// be assigned an empty default value.
  factory Article.fromJson( Map<String, dynamic> json )
  {

    String url = json[ 'url' ] ?? '';

    if ( url.isEmpty )
    {
      url = 'https://news.ycombinator.com/item?id=${ json[ "id" ] }';
    }

    return Article(
      id: json[ 'id' ] ?? 1,
      score: json[ 'score' ] ?? 0,
      title: json[ 'title' ] ?? '',
      type: json[ 'type' ] ?? '',
      url: url,
    );

  }

  /* ---------------------------------- */
  /* Static methods */

  /* ---------------------------------- */
  /* fetch */

  /// Attempts to return an [Article] containing the metadata for [articleID].
  ///
  /// Returns `null` if the information could not be retrieved.
  static Future<Article?> fetch( int articleID ) async
  {

    final response = await http.get( Uri.parse( 'https://hacker-news.firebaseio.com/v0/item/$articleID.json' ) );

    if ( response.statusCode == 200 )
    {
      return Article.fromJson( jsonDecode( response.body ) );
    }
    else
    {
      return null;
    }

  }

}

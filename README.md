# <img src="assets/icon_shaped_square.png" width=75> Hackles

A distractionless Hacker News article browser for Android.

Built with [Flutter](https://flutter.dev/).

[![Build Status](https://gitlab.com/jessieh/hackles/badges/main/pipeline.svg)](https://gitlab.com/jessieh/hackles/-/pipelines)

## Screenshots

<img src="assets/screenshots/light.png" width=250> <img src="assets/screenshots/dark.png" width=250>

## Downloads

[<img src="assets/repo/release.png" width=250>](https://gitlab.com/jessieh/hackles/-/jobs/artifacts/main/raw/Hackles.apk?job=build-release)

[<img src="assets/repo/debug.png" width=250>](https://gitlab.com/jessieh/hackles/-/jobs/artifacts/main/raw/Hackles-debug.apk?job=build-debug)

## Usage

* Tapping the refresh button will check the front page of Hacker News for any changes.

* Tapping a post will open the linked article using the default handler for that type of link.

* Long pressing a post will open a share sheet for the linked article so that you can (*e.g.*) save it to [Pocket](https://getpocket.com) or send it to a friend.
